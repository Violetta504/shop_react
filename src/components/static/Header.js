import React, {Component} from "react";
import { Link } from 'react-router-dom';

import Cart from "../modules/Cart";

const menu = require('../../system/menu.json');

export default class Header extends Component {

    showMenu(){
        return menu.map((el, k) => <li className="nav-item" key={k} ><Link className="nav-link" to={el.link}>{el.name}</Link></li>);
    }

    render()
    {
        return (
            <header>
                <nav className="nav">
                    {this.showMenu()}
                    <Cart productsCart={this.props.productsCart}/>
                </nav>
            </header>
        )
    }
}