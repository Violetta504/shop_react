import React, { Component } from "react";

export default class CartProduct extends Component {

    render() {
        return (
            <tr>
                <th>{this.props.data.name}</th>
                <th><img className="img-cart" src={this.props.data.image} /></th>
                <th>{this.props.data.price}</th>
                <th>{this.props.data.manufacturer}</th>
            </tr>
        )
    }
}