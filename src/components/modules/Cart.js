import React, { Component } from "react";
import CartProduct from "./CartProduct";

import {connect} from "react-redux";

class Cart extends Component {

    showProducts = () => {
        return this.props.products.map((el, key)=> <CartProduct data = {el} key = {key} />);
    }

    render() {
        return (
            <div className="cart col-md-4">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Total Price</th>
                            <th></th>
                            <th>Total Quantity</th>
                            <th>10</th>
                        </tr>
                    </thead>
                </table>
               <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Quantity</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                       {this.showProducts()}
                    </tbody>
               </table>
            </div>
        )
    }
}

export default connect(
    state => ({
        products:state.cart.products
    }),
    dispatch => ({
        removeProduct: (d)=>{
            
        }
    })
)(Cart);
