import React, { Component } from "react";
import {connect} from "react-redux";

class Product extends Component {

    add = () => {
        this.props.addCart(this.props.data);
    }

    render() {
        return (
            <div className="product col-md-4">
                <div className="prod-wrap">
                    <h4>{this.props.data.name}</h4>
                    <div className="img-block">
                        <img src={this.props.data.image} className="img-fluid" />
                    </div>
                    <div>
                        {this.props.data.price}
                    </div>
                    <div>
                        {this.props.data.manufacturer}
                    </div>
                    <button data-pkey={this.props.pkey} onClick = {this.add} type="button" className="btn btn-primary">Add to cart</button>
                </div>
            </div>
        )
    }
}


export default connect(
    state => ({
    
    }),
    dispatch => ({
        addCart: (d)=>{
            dispatch({type:"ADD_PRODUCT", data:d})
        }
    })
)(Product);