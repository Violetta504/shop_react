import React, {Component} from "react";
import Product from "../modules/Product";
import {connect} from "react-redux";


class Products extends Component{
    showProducts = () => {
        return this.props.products.map((product, key)=> <Product addCart = {this.props.addCart} key={key} pkey={key} data={product} />)
    }

    render(){
        return (
            <div className="page">
                <div className="row">
                    {this.showProducts()}
                </div>
            </div>
        )
    }
}

  
export default connect(
    state => ({
        products:state.app.products
    }),
    dispatch => ({

    })
)(Products);