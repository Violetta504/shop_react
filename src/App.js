import React, {Component} from "react";

import Footer from "./components/static/Footer";
import Header from "./components/static/Header";
import Router from "./Router";
import styles from "./App.css";

const dataProducts = require('./lib/products.json');

export default class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            productsCart: [],
            products: dataProducts,
            totalPrice: 0,
            totalQuantity: 0
        }
    }

    addCart = (elem) => {
        console.log(elem.target);
        let index = elem.target.dataset.pkey;
        console.log(index);

        let newProductsCart = [...this.state.productsCart, {...this.state.products[index]}];

        this.setState({productsCart: newProductsCart})
        
    }

    render()
    {
        return (
            <div className="app-container container">
                <Header productsCart = {this.state.productsCart} />
                <Router addCart = {this.addCart} products={this.state.products} />
                <Footer />
            </div>
        )
    }
}