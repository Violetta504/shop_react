const dataProducts = require('../lib/products.json');

const initState = {
    products:dataProducts
}

export default function app(state = initState, action){
    switch(action.type){
        case "ADD_PRODUCT":
        return {
            ...state,
            num: action.data
        }
    }
    return state;
}