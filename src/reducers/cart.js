
const initState = {
    products:[],
    totalSum:0,
    totalQuantity:0
}
export default function app(state = initState, action){
    switch(action.type){
        case "ADD_PRODUCT":
        return {
            ...state,
            products:[...state.products, {...action.data}]
        }
    }
    return state;
}