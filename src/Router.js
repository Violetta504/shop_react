import React from 'react';

import { Route, Switch } from 'react-router-dom';

import Home from './components/pages/Home';
import Login from './components/pages/Login';
import Registration from './components/pages/Registration';
import Contacts from './components/pages/Contacts';
import Products from './components/pages/Products';


export default (props) => (
    <Switch>
        <Route exact path='/' component={Home}/>
        <Route path='/login'  component={Login}/>
        <Route path='/registration' component={Registration}/>
        <Route path='/contacts' component={Contacts} />
        <Route path='/products' component={Products} />
    </Switch>
);